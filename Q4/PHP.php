<?php
    $message = "";
    $num = 0;
    if(isset($_POST['show'])){
        $num = $_POST['nm']; 
        $ans = 1;
        $m = $num;

        for ($i=0; $i < $num ; $i++) { 
            $ans = $ans*$m;  
            $m=$m-1;  
        }
    $message = "The factorial of number ".$num." is ".$ans;
    } 
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Factorial using php</title>
</head>
<body>
    <form action="" method="post">
        <p>How to find a factorial of a number ?</p>
        <input type='number' name='nm' value = "<?php echo $num; ?>">
        <input type='submit' name='show' value='Show'>
        <br><br>
        <?php echo $message; ?>
    </form>
</body>
</html>



